
# Construction des images docker CDash 

On récupère les sources

```
git clone https://github.com/Kitware/CDash.git
```

Puis dans CDash AVEC le fichier [docker.cmake](./dockerfiles/CDash/docker/cdash.docker) modifié et fourni dans ce projet :

```
docker build . -f docker/cdash.docker --target=cdash-worker -t cdash-worker-v3.2c
docker build . -f docker/cdash.docker --target=cdash -t cdash-v3.2c
docker build . -f docker/cdash.docker --target=cdash-debug -t cdash-debug-v3.2c

docker tag cdash-v3.2c  gricad-registry.univ-grenoble-alpes.fr/scalde-contrib/cdash-openshift/mycdash:v3.2.0
docker tag cdash-worker-v3.2c  gricad-registry.univ-grenoble-alpes.fr/scalde-contrib/cdash-openshift/mycdash-worker:v3.2.0
docker tag cdash-debug-v3.2c  gricad-registry.univ-grenoble-alpes.fr/scalde-contrib/cdash-openshift/mycdash-debug:v3.2.0

docker push gricad-registry.univ-grenoble-alpes.fr/scalde-contrib/cdash-openshift/mycdash:v3.2.0
docker push gricad-registry.univ-grenoble-alpes.fr/scalde-contrib/cdash-openshift/mycdash-worker:v3.2.0
docker push gricad-registry.univ-grenoble-alpes.fr/scalde-contrib/cdash-openshift/mycdash-debug:v3.2.0
```


# Déploiement sur la plm


```
./make-cdash-v3.2.sh scalde-dashboard.apps.math.cnrs.fr
```


